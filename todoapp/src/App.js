import React, { Component } from 'react';
import Todos from './todos';
import AddForm from './AddForm'

class App extends Component {
  state = {
    todos: [
      {id: 1, content: "workout"},
      {id: 2, content: "code"}
    ]
  }
  deleteTodo = (id) => {
    let todos = this.state.todos.filter(todo => {
      return todo.id !== id
    })

    this.setState({
      todos:todos
    })
  }
  AddTodo = (todo) => {
    todo.id = Math.random()
    let todos = [...this.state.todos, todo]

    this.setState({
      todos:todos
    })
  }
  render() {
    return (
      <div className="todo-app container">
        <h1 className="center blue-text">Todo's with Reactjs</h1>
        <p className="center blue-text">by cholo</p>
        <AddForm addTodo={this.AddTodo}/>
        <Todos todos={this.state.todos} deleteTodo={this.deleteTodo}/>
      </div>
    );
  }
}

export default App;

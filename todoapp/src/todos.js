import React from 'react';

const Todos = ({todos, deleteTodo}) => {
    const todoList = todos.length ? (
        todos.map(todo => {
            return(
                <div className="collection-item" key={todo.id}>
                    <span>{todo.content}</span>
                    <i onClick={() => {deleteTodo(todo.id)}} className="small clear right">x</i>
                </div>
            )
        })
    ) : (
        <p className="center">You have no tasks! Go to Adam</p>
    )
    return(
        <div className="todos collection">
            {todoList}
        </div>
    )
}

export default Todos